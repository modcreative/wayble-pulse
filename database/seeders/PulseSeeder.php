<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use WayblePulse\Models\Category;
use WayblePulse\Models\Signal;

class PulseSeeder extends Seeder
{
    public function run(): void
    {
        // categories and goals
        $maintenance = Category::create([
            'name'        => 'Maintenance',
            'description' => '',
        ]);

        $maintenanceGoal = $maintenance->goals()->create([
            'name' => 'Maintain clean and well-stocked restrooms',
        ]);

        $restaurants = Category::create([
            'name'        => 'Restaurants',
            'description' => '',
        ]);

        $restaurantGoal = $restaurants->goals()->create([
            'name' => 'Ensure guests can find food options',
        ]);

        // signals
        Signal::create([
            'category_id'       => $maintenance->id,
            'goal_id'           => $maintenanceGoal->id,
            'name'              => 'Maintenance request',
            'frequency_count'   => 2,
            'frequency_minutes' => 10,
            'actions'           => [
                [
                    'type'    => 'display_message',
                    'message' => 'Restrooms will be cleaned and stocked ASAP.',
                ]
            ]
        ]);

        Signal::create([
            'category_id'       => $restaurants->id,
            'goal_id'           => $restaurantGoal->id,
            'name'              => 'Food options',
            'frequency_count'   => 2,
            'frequency_minutes' => 10,
            'actions'           => [
                [
                    'type'    => 'display_message',
                    'message' => 'Restaurants and food options located on the 2nd floor.',
                ]
            ]
        ]);
    }
}
