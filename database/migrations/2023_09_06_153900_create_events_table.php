<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('events', function (Blueprint $table)
        {
            $table->uuid('id')->primary();
            $table->foreignUuid('trainable_id')->nullable();
            $table->foreignUuid('category_id')->nullable();
            $table->foreignUuid('goal_id')->nullable();
            $table->text('summary');
            $table->string('tags')->nullable();
            $table->json('data')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('events');
    }
};
