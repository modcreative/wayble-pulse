<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        if (Schema::hasColumns('events', ['summary', 'was_reply_successful']))
        {
            Schema::table('events', function (Blueprint $table) {
                $table->dropColumn('summary');
                $table->dropColumn('was_reply_successful');
            });
        }
    }

    public function down(): void
    {
        if (!Schema::hasColumns('events', ['summary', 'was_reply_successful']))
        {
            Schema::table('events', function (Blueprint $table) {
                $table->after('goal_id', function (Blueprint $table) {
                    $table->text('summary')->nullable();
                    $table->boolean('was_reply_successful')->nullable();
                });
            });
        }
    }
};
