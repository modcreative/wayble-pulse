<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('signal_hits', function (Blueprint $table)
        {
            $table->id();
            $table->foreignUuid('signal_id');
            $table->foreignUuid('event_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('signal_hits');
    }
};
