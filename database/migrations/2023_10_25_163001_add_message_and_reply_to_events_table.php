<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('events', function (Blueprint $table) {
            $table->after('summary', function (Blueprint $table) {
                $table->text('message')->nullable();
                $table->text('reply')->nullable();
                $table->boolean('was_reply_successful')->nullable();
            });
        });
    }

    public function down(): void
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('message');
            $table->dropColumn('reply');
            $table->dropColumn('was_reply_successful');
        });
    }
};
