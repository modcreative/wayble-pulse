<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('signals', function (Blueprint $table)
        {
            $table->uuid('id')->primary();
            $table->foreignUuid('category_id')->nullable();
            $table->foreignUuid('goal_id')->nullable();
            $table->string('name');
            $table->integer('frequency_count')->nullable();
            $table->integer('frequency_minutes')->nullable();
            $table->json('additional_trigger_conditions')->nullable();
            $table->json('actions')->nullable();
            $table->dateTime('processed_at')->nullable();

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('signals');
    }
};
