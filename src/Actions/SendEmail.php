<?php

namespace WayblePulse\Actions;

use WayblePulse\Mail\SignalTriggerMail;
use WayblePulse\Models\Signal;
use Illuminate\Support\Facades\Mail;

class SendEmail extends BaseAction
{
    public string $email;
    public string $body;

    public function __construct(
        public array $action,
        public Signal $signal,
    )
    {
        $this->email = $this->action['email'];
        $this->body  = $this->action['message'];
    }

    public function perform(): void
    {
        Mail::to($this->email)->send(new SignalTriggerMail($this->body));
    }
}