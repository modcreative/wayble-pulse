<?php

namespace WayblePulse\Actions;

use WayblePulse\Events\ShowImageEvent;
use WayblePulse\Models\Signal;
use Illuminate\Support\Facades\Storage;

class DisplayImage extends BaseAction
{
    public string $image;

    public function __construct(
        public array  $action,
        public Signal $signal,
    )
    {
        $this->image = asset(Storage::url($this->action['image']));
    }

    public function perform(): void
    {
        ShowImageEvent::dispatch($this->image);
    }
}