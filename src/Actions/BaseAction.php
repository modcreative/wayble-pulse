<?php

namespace WayblePulse\Actions;

use WayblePulse\Models\Signal;
use Illuminate\Support\Str;

abstract class BaseAction
{
    public static function make(array $action, Signal $signal)
    {
        $actionClass = '\\WayblePulse\\Actions\\' . Str::studly($action['type']);

        if (!class_exists($actionClass))
        {
            throw new \Exception("Action class {$actionClass} does not exist.");
        }

        return new $actionClass($action, $signal);
    }

    abstract public function perform(): void;
}