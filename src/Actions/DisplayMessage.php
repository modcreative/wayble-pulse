<?php

namespace WayblePulse\Actions;

use WayblePulse\Events\ShowMessageEvent;
use WayblePulse\Models\Signal;

class DisplayMessage extends BaseAction
{
    public string $message;

    public function __construct(
        public array  $action,
        public Signal $signal,
    )
    {
        $this->message  = $this->action['message'];
    }

    public function perform(): void
    {
        ShowMessageEvent::dispatch($this->message);
    }
}