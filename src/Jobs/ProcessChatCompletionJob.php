<?php

namespace WayblePulse\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use WayblePulse\Services\AnalyticsService;
use WayblePulse\Services\ClassificationService;

class ProcessChatCompletionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        public string $message,
        public string $reply
    ) {}

    public function handle(): void
    {
        try
        {
            $classifyService  = new ClassificationService($this->message, $this->reply);
            $analyticsService = new AnalyticsService();

            $classifiedMessage = $classifyService->classifyChatMessage();

            Log::info('[Pulse] message classification', ['classification' => $classifiedMessage]);

            $event = $analyticsService->recordEvent($this->message, $this->reply, $classifiedMessage);

            $analyticsService->recordSignalHit($event);
        }
        catch (\Exception $e)
        {
            Log::error($e->getMessage());
            return;
        }

    }
}
