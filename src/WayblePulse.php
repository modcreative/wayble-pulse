<?php

namespace WayblePulse;

/**
 * @method static void processIncomingChatCompletion(string $message, string $reply)
 */
class WayblePulse extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor(): string
    {
        return WayblePulseApi::class;
    }
}
