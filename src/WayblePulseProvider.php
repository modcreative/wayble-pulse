<?php

namespace WayblePulse;

use Illuminate\Support\Facades\Event;

class WayblePulseProvider extends \Illuminate\Support\ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(WayblePulseApi::class, static fn() => new WayblePulseApi());
    }

    public function provides(): array
    {
        return [
            WayblePulseApi::class,
        ];
    }

    public function boot(): void
    {
        $this->registerConfig();
        $this->registerMigrations();
        $this->registerCommands();
        $this->registerListeners();
        $this->registerPublishing();
    }

    public function registerConfig(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/wayble-pulse.php', 'wayble-pulse');
    }

    public function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    public function registerCommands(): void
    {
        if ($this->app->runningInConsole())
        {
            $this->commands([
                //
            ]);
        }
    }

    public function registerListeners(): void
    {
        Event::listen(
            \WaybleAI\Events\ChatReplyEvent::class,
            \WayblePulse\Listeners\ChatReplyListener::class
        );
    }

    public function registerPublishing(): void
    {
        $tags = [
            'wayble-pulse-config' => [
                __DIR__ . '/../config/wayble-pulse.php' => config_path('wayble-pulse.php')
            ],
            'wayble-pulse-database' => [
                __DIR__ . '/../database/migrations' => database_path('migrations'),
                __DIR__ . '/../database/seeders' => database_path('seeders'),
            ],
            'wayble-pulse-filament' => [
                __DIR__ . '/../filament/Resources' => app_path('Filament/Resources'),
                __DIR__ . '/../filament/Widgets' => app_path('Filament/Widgets'),
            ],
        ];

        foreach ($tags as $key => $value)
        {
            $this->publishes($value, $key);
        }

        $this->publishes(array_merge(...array_values($tags)), 'wayble-pulse');
    }
}
