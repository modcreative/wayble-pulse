<?php

namespace WayblePulse;

use WayblePulse\Jobs\ProcessChatCompletionJob;

class WayblePulseApi
{
    public function processIncomingChatCompletion(string $message, string $reply): void
    {
        ProcessChatCompletionJob::dispatch($message, $reply);
    }
}
