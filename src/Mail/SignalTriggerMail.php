<?php

namespace WayblePulse\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SignalTriggerMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        public string $body,
    ) {}

    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Signal Trigger',
        );
    }

    public function content(): Content
    {
        return new Content(
            view: 'emails.signal-trigger',
        );
    }

    public function attachments(): array
    {
        return [];
    }
}
