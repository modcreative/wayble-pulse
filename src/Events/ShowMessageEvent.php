<?php

namespace WayblePulse\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ShowMessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(
        public string $message,
    ) {}

    public function broadcastAs(): string
    {
        return 'pulse.show.message';
    }

    public function broadcastOn(): array
    {
        return [
            new Channel(sprintf('%s_bulletins', config('wayble-pulse.pusher.namespace'))),
        ];
    }
}
