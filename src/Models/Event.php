<?php

namespace WayblePulse\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasUuids;

    protected $casts = [
        'data' => 'array',
    ];

    protected $fillable = [
        'category_id',
        'goal_id',
        'message',
        'reply',
        'tags',
        'data',
    ];

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function goal(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Goal::class);
    }

    public function matchSignals(): \Illuminate\Database\Eloquent\Collection
    {
        return Signal::where('category_id', $this->category_id)
            ->where('goal_id', $this->goal_id)
            ->get();
    }
}
