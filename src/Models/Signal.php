<?php

namespace WayblePulse\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use WayblePulse\Actions\BaseAction;

class Signal extends Model
{
    use HasUuids;

    protected $fillable = [
        'category_id',
        'goal_id',
        'frequency_count',
        'frequency_minutes',
        'name',
        'triggers',
        'actions',
        'processed_at',
    ];

    protected $casts = [
        'triggers'     => 'array',
        'actions'      => 'array',
        'processed_at' => 'datetime',
    ];

    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function goal(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Goal::class);
    }

    public function hits(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SignalHit::class);
    }

    public function isReadyForProcessing(): bool
    {
        return $this->hasCrossedTimeThreshold() && $this->hasCrossedHitThreshold();
    }

    public function hasCrossedTimeThreshold(): bool
    {
        if (!$this->processed_at)
        {
            return true;
        }

        return $this->processed_at->diffInMinutes(now()) > $this->frequency_minutes;
    }

    public function hasCrossedHitThreshold(): bool
    {
        return $this->getHitCountWithinTimeframe() >= $this->frequency_count;
    }

    public function getHitCountWithinTimeframe(): int
    {
        $timeStart = now()->subMinutes($this->frequency_minutes);
        $timeEnd   = now();

        return $this->hits()
            ->whereBetween('created_at', [$timeStart, $timeEnd])
            ->count();
    }
}
