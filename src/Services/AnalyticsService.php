<?php

namespace WayblePulse\Services;

use WayblePulse\Models\Category;
use WayblePulse\Models\Event;
use WayblePulse\Models\Goal;
use WayblePulse\Models\Signal;

class AnalyticsService
{
    public function recordEvent(string $message, string $reply, array $messageData): Event
    {
        return Event::create([
            'category_id' => Category::where('name', $messageData['category'])->first()?->id,
            'goal_id' => Goal::where('name', $messageData['goal'])->first()?->id,
            'message' => $message,
            'reply' => $reply,
            'tags' => implode(',', $messageData['tags'] ?? []),
            'data' => $messageData,
        ]);
    }

    public function recordSignalHit(Event $event): void
    {
        $signals = $event->matchSignals();

        if ($signals->isEmpty())
        {
            return;
        }

        $signals->each(function (Signal $signal) use ($event) {
            $signal->hits()->create([
                'event_id' => $event->id,
            ]);

            $signalService = new SignalService($signal);

            $signalService->triggerAction();
        });
    }
}
