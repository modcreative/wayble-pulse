<?php

namespace WayblePulse\Services;

use Illuminate\Support\Facades\Log;
use WayblePulse\Actions\BaseAction;
use WayblePulse\Models\Signal;

class SignalService
{
    public function __construct(
        protected Signal $signal,
    ) {}

    public function triggerAction(): void
    {
        if (!$this->signal->isReadyForProcessing())
        {
            $processedAt = $this->signal->processed_at ?? now();

            Log::info('Signal ' . $this->signal->id . ' is not ready to be processed yet.');
            Log::info('Signal ' . $this->signal->id . '. Last processed at: ' . $this->signal->processed_at. '. Can process again at: ' . $processedAt->addMinutes($this->signal->frequency_minutes));
            Log::info('Signal ' . $this->signal->id . '. Actual hits: ' . $this->signal->getHitCountWithinTimeframe() . ' Required: ' . $this->signal->frequency_count);

            return;
        }

        foreach ($this->signal->actions as $actionItem)
        {
            try
            {
                $action = BaseAction::make($actionItem, $this->signal);

                $action->perform();

                Log::info('Signal ' . $this->signal->id . ' processed.', $actionItem);
            }
            catch (\Exception $e)
            {
                Log::error($e->getMessage());
            }

        }

        $this->signal->update(['processed_at' => now()]);
    }
}