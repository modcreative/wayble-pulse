<?php

namespace WayblePulse\Services;

use Exception;
use WaybleAI\Factories\ChatModelFactory;
use WayblePulse\Models\Category;
use WayblePulse\Models\Goal;

class ClassificationService
{
    public function __construct(
        public string $message,
        public string $reply
    )
    {
    }

    /**
     * @throws Exception
     */
    public function classifyChatMessage(): ?array
    {
        $model = ChatModelFactory::make(params: [
            'maxTokens' => 1500,
            'temperature' => 0,
        ]);

        $messages = $model->chat([
            $model->makeMessage('system', $this->messagePrompt()),
            $model->makeMessage('user', $this->message),
        ]);

        $assistantMessage = array_pop($messages);

        $response = $assistantMessage->content;

        if (json_decode($response) === null)
        {
            throw new Exception('Response must be in json format.' . PHP_EOL . $response);
        }

        return json_decode($response, true);
    }

    public function messagePrompt(): string
    {
        $categories = Category::all()->pluck('name')->implode(PHP_EOL);
        $goals = Goal::all()->pluck('name')->implode(PHP_EOL);

        return <<<EOT
            First, determine which category best represents the user message, it may be none. (key: category)
            Second, determine which goal best represents the user message, it may be none. (key: goal)
            Next, classify the user message into Sentiment. (key: sentiment)
            Next, classify the user message into Tone. (key: tone)
            Next, create up to 3 relevant tags based on the user message, please keep tags short and two words max. (key: tags)
            Finally, return ONLY in json format.

            Categories:
            $categories

            Goals:
            $goals
            EOT;
    }
}
