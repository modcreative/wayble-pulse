<?php

namespace WayblePulse\Listeners;

use WaybleAi\Events\ChatReplyEvent;
use WayblePulse\WayblePulse;

class ChatReplyListener
{
    public function __construct() {}

    public function handle(ChatReplyEvent $event): void
    {
        WayblePulse::processIncomingChatCompletion($event->userMessage->content, $event->assistantMessage->content);
    }
}
