<?php

return [
    'pusher' => [
        'namespace' => env('PUSHER_APP_NAMESPACE', 'pulse'),
    ],

    'twilio' => [
        'sid' => env('TWILIO_ACCOUNT_SID'),
        'token' => env('TWILIO_AUTH_TOKEN'),
        'phone' => env('TWILIO_PHONE_NUMBER'),
        'whatsapp' => env('TWILIO_WHATSAPP_NUMBER'),
    ],
];
