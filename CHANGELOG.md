# Wayble Pulse for Laravel
AI Analytics framework for Laravel
---

## [2.0.0] - 2023-11-03

### Updated
- Updated Openai package to support Wayble AI V2

---

## [1.1.2] - 2023-10-26

### Fixed
- Fixed issue for comparing hit threshold

---

## [1.1.1] - 2023-10-26

### Fixed
- Fixed exception issue and surround call in try/catch

---

## [1.1.0] - 2023-10-26

### Added
- Added assistant reply classification

### Updated
- Updated migrations to events table to support message, reply, and boolean `was_reply_successful`

---

## [1.0.1] - 2023-10-23

### Added
- Added pusher namespace config option, defaults to `pulse`

---

## [1.0.0] - 2023-10-05

### Added
- Added first version of Wayble Pulse
