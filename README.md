# Wayble Pulse for Laravel
AI Analytics framework for Laravel

## Usage

### Install
```bash
# install package
composer require modcreative/laravel-wayble-pulse

# export migrations
php artisan vendor:publish --tag="wayble-pulse-database"
```

### Configure
```bash
# optionally export config
php artisan vendor:publish --tag="wayble-pulse-config"

# edit config
config/wayble-pulse.php
```

### Filament
> If you're using filament in your Laravel app, you can export the resources provided by Wayble Pulse.
>
```bash
# export filament resources
php artisan vendor:publish --tag="wayble-pulse-filament"
```
> If you wish to add certain or all widgets to your dashboard you must update your admin panel provider after exporting the filament resources from the package (see above).
```php
# app/Providers/Filament/AdminPanelProvider.php
return $panel
    ->widgets([
        \App\Filament\Widgets\Cards\ChattingStats::class,
        \App\Filament\Widgets\Charts\SignalHitsByCategory::class,
        \App\Filament\Widgets\Charts\TotalMessages::class,
        \App\Filament\Widgets\Tables\RecentMessages::class,
    ])
```

### Kitchen Sink
> Want to publish everything together and overwrite whatever already exists?
>
```bash
# export and overwrite all publishables
php artisan vendor:publish --tag="wayble-pulse" --force
```

## Gotchas 💣
> We use `uuids` everywhere!
