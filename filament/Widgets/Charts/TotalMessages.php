<?php

namespace App\Filament\Widgets\Charts;

use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class TotalMessages extends BaseChart
{
    protected static ?string $heading = 'Total Messages';

    protected function getData(): array
    {
        $data = Trend::model(config('ai.models.message'))
            ->between($this->getRangeStart(), $this->getRangeEnd())
            ->interval($this->mapFilterToInterval())
            ->count();

        $color = $this->getChartColor();

        return [
            'datasets' => [
                [
                    'label'           => 'Messages',
                    'data'            => $data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor'     => $color,
                    'backgroundColor' => $color,
                ],
            ],
            'labels' => $data->map(
                fn(TrendValue $value) => $this->getLabelForFilteredDate($value->date)
            ),
        ];
    }
}
