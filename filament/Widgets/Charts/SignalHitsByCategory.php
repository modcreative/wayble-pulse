<?php

namespace App\Filament\Widgets\Charts;

use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;
use WayblePulse\Models\Category;
use WayblePulse\Models\SignalHit;

class SignalHitsByCategory extends BaseChart
{
    protected static ?string $heading = 'Signal Hits By Category';

    protected function getData(): array
    {
        $categories = Category::all();

        if (empty($categories))
        {
            return [];
        }

        $signalHits = SignalHit::select('id');

        if (empty($signalHits))
        {
            return [];
        }

        $datasets = [];
        $labels   = [];

        foreach ($categories as $index => $category)
        {
            $signalHitsQuery = SignalHit::whereHas('event', function ($query) use ($category) {
                $query->where('category_id', $category->id);
            });

            $trend = Trend::query($signalHitsQuery)
                ->between($this->getRangeStart(), $this->getRangeEnd())
                ->interval($this->mapFilterToInterval())
                ->count();

            $color = $this->getChartColor($index);

            $datasets[] = [
                'label'           => $category->name,
                'data'            => $trend->map(fn(TrendValue $value) => $value->aggregate),
                'borderColor'     => $color,
                'backgroundColor' => $color,
            ];
        }

        if (isset($trend))
        {
            $labels = $trend->map(
                fn(TrendValue $value) => $this->getLabelForFilteredDate($value->date)
            );
        }

        return [
            'datasets' => $datasets,
            'labels'   => $labels
        ];
    }
}
