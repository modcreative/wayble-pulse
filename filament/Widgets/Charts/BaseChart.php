<?php

namespace App\Filament\Widgets\Charts;

use Carbon\Carbon;
use Filament\Widgets\ChartWidget;

class BaseChart extends ChartWidget
{
    const DEFAULT_CHART_COLORS = [
        '#f87171',
        '#fb923c',
        '#facc15',
        '#a3e635',
        '#4ade80',
        '#2dd4bf',
        '#22d3ee',
        '#60a5fa',
        '#818cf8',
        '#c084fc',
        '#f472b6',
    ];

    public ?string $filter = 'today';

    protected function getOptions(): array
    {
        return [
            'colors' => self::DEFAULT_CHART_COLORS,
            'tension' => 0.3,
            'scales' => [
                'y' => [
                    'min' => 0
                ]
            ]
        ];
    }

    protected function getData(): array
    {
        return [];
    }

    protected function getType(): string
    {
        return 'line';
    }

    protected function getFilters(): array
    {
        return [
            'today'         => 'Today',
            'week'          => 'Last Week',
            'current_month' => 'This Month',
            'month'         => 'Last Month',
            'year'          => 'This Year'
        ];
    }

    protected function getRangeStart()
    {
        return match($this->filter)
        {
            'today'         => now()->startOfDay(),
            'week'          => now()->subWeek()->startOfWeek(),
            'current_month' => now()->startOfMonth(),
            'month'         => now()->subMonth()->startOfMonth(),
            'year'          => now()->startOfYear(),
            default         => throw new \Exception('Invalid filter value')
        };
    }

    protected function getRangeEnd()
    {
        return match($this->filter)
        {
            'today'         => now()->endOfDay(),
            'week'          => now()->subWeek()->endOfWeek(),
            'current_month' => now()->endOfMonth(),
            'month'         => now()->subMonth()->endOfMonth(),
            'year'          => now()->endOfYear(),
            default         => throw new \Exception('Invalid filter value')
        };
    }

    protected function mapFilterToInterval()
    {
        return match($this->filter)
        {
            'today'         => 'hour',
            'week',
            'current_month',
            'month'         => 'day',
            'year'          => 'month',
            default         => throw new \Exception('Invalid filter value')
        };
    }

    protected function getLabelForFilteredDate(string $date)
    {
        $date = new Carbon($date);

        return match($this->filter)
        {
            'today'         => $date->format('g:i A'),
            'week'          => $date->format('D n/j'),
            'current_month',
            'month'         => $date->format('n/j'),
            'year'          => $date->format('M'),
            default         => throw new \Exception('Invalid filter value')
        };
    }

    protected function getChartColor(int $index = 0)
    {
        return $this->getOptions()['colors'][$index];
    }
}