<?php

namespace App\Filament\Widgets\Tables;

use Filament\Tables;
use Filament\Tables\Table;
use Filament\Widgets\TableWidget as BaseWidget;

class RecentMessages extends BaseWidget
{
    protected int|string|array $columnSpan = 'full';

    public function table(Table $table): Table
    {
        return $table
            ->query(
                \App\Filament\Resources\MessageResource::getEloquentQuery()
            )
            ->defaultPaginationPageOption(5)
            ->defaultSort('created_at', 'desc')
            ->columns([
                Tables\Columns\TextColumn::make('role')
                    ->sortable()
                    ->badge(),
                Tables\Columns\TextColumn::make('content')
                    ->searchable()
                    ->words(12)
                    ->wrap(),
                Tables\Columns\TextColumn::make('chat_id')
                    ->label('Chat')
                    ->sortable()
                    ->searchable()
                    ->badge(),
                Tables\Columns\TextColumn::make('created_at')
                    ->label('Sent at')
                    ->dateTime('M j, Y, g:i A')
            ]);
    }
}