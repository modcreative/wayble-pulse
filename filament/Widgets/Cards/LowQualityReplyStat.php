<?php

namespace App\Filament\Widgets\Cards;

use Filament\Widgets\StatsOverviewWidget\Stat;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class LowQualityReplyStat
{
    public static function make()
    {
        $model = config('ai.models.message');

        $lowQuality     = $model::lowQualityReplies()->count();
        $lowQualityPrev = $model::lowQualityReplies()->fromLastMonth()->count();

        $lowQualityChange  = self::calculateChange($lowQualityPrev, $lowQuality);

        $lowQualityTrend = Trend::query($model::lowQualityReplies())
            ->between(
                start: now()->startOfMonth(),
                end: now()->endOfMonth()
            )
            ->perDay()
            ->count();

        return Stat::make('Low Quality Replies', $lowQuality)
            ->description($lowQualityChange)
            ->descriptionIcon(self::calculateIcon($lowQualityChange))
            ->color(self::calculateColor($lowQualityChange))
            ->chart(
                $lowQualityTrend
                    ->map(fn (TrendValue $value) => $value->aggregate)
                    ->toArray()
            );
    }

    private static function calculateChange(int $value1, int $value2): string
    {
        $diff = $value2 - $value1;

        if ($diff === 0)
        {
            return 'No change';
        }

        if ($value1 === 0 && $diff > 0)
        {
            return sprintf('Increase from 0 to %d', $value2);
        }

        $change = abs($diff / $value1) * 100;

        if ($diff > 0)
        {
            return sprintf('%d%% increase', $change);
        }

        return sprintf('%d%% decrease', $change);
    }

    private static function calculateColor(string $change): string
    {
        if (str_contains(strtolower($change), 'increase'))
        {
            return 'danger';
        }

        if (str_contains(strtolower($change), 'decrease'))
        {
            return 'success';
        }

        return 'gray';
    }

    private static function calculateIcon(string $change): string
    {
        if (str_contains(strtolower($change), 'increase'))
        {
            return 'heroicon-m-arrow-trending-up';
        }

        if (str_contains(strtolower($change), 'decrease'))
        {
            return 'heroicon-m-arrow-trending-down';
        }

        return '';
    }
}
