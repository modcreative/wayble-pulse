<?php

namespace App\Filament\Widgets\Cards;

use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Stat;

class OverviewStats extends BaseWidget
{
    protected function getStats(): array
    {
        return [
            LowQualityReplyStat::make(),
            HighQualityReplyStat::make(),
            TokensUsedStat::make(),
        ];
    }
}
