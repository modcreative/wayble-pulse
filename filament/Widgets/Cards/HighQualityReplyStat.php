<?php

namespace App\Filament\Widgets\Cards;

use Filament\Widgets\StatsOverviewWidget\Stat;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class HighQualityReplyStat
{
    public static function make()
    {
        $model = config('ai.models.message');

        $highQuality     = $model::highQualityReplies()->count();
        $highQualityPrev = $model::highQualityReplies()->fromLastMonth()->count();

        $highQualityChange = self::calculateChange($highQualityPrev, $highQuality);

        $highQualityTrend = Trend::query($model::highQualityReplies())
            ->between(
                start: now()->startOfMonth(),
                end: now()->endOfMonth()
            )
            ->perDay()
            ->count();

        return Stat::make('High Quality Replies', $highQuality)
            ->description($highQualityChange)
            ->descriptionIcon(self::calculateIcon($highQualityChange))
            ->color(self::calculateColor($highQualityChange))
            ->chart(
                $highQualityTrend
                    ->map(fn (TrendValue $value) => $value->aggregate)
                    ->toArray()
            );
    }

    private static function calculateChange(int $value1, int $value2): string
    {
        $diff = $value2 - $value1;

        if ($diff === 0)
        {
            return 'No change';
        }

        if ($value1 === 0 && $diff > 0)
        {
            return sprintf('Increase from 0 to %d', $value2);
        }

        $change = abs($diff / $value1) * 100;

        if ($diff > 0)
        {
            return sprintf('%d%% increase', $change);
        }

        return sprintf('%d%% decrease', $change);
    }

    private static function calculateColor(string $change): string
    {
        if (str_contains(strtolower($change), 'increase'))
        {
            return 'success';
        }

        if (str_contains(strtolower($change), 'decrease'))
        {
            return 'danger';
        }

        return 'gray';
    }

    private static function calculateIcon(string $change): string
    {
        if (str_contains(strtolower($change), 'increase'))
        {
            return 'heroicon-m-arrow-trending-up';
        }

        if (str_contains(strtolower($change), 'decrease'))
        {
            return 'heroicon-m-arrow-trending-down';
        }

        return '';
    }
}