<?php

namespace App\Filament\Resources;

use App\Filament\Resources\SignalResource\Pages;
use App\Filament\Resources\SignalResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables\Table;
use Filament\Tables;
use WaybleAI\Models\Trainable;
use WayblePulse\Models\Category;
use WayblePulse\Models\Goal;
use WayblePulse\Models\Signal;

class SignalResource extends Resource
{
    protected static ?string $model = Signal::class;

    protected static ?string $navigationIcon = 'heroicon-o-megaphone';

    protected static ?string $navigationGroup = 'Wayble Pulse';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Section::make('Signal')
                    ->schema([
                        Forms\Components\TextInput::make('name')
                            ->required(),
                    ]),
                Forms\Components\Section::make('Trigger')
                    ->columns(2)
                    ->schema([
                        Forms\Components\Select::make('category_id')
                            ->label('Category')
                            ->options(Category::all()->pluck('name', 'id'))
                            ->reactive()
                            ->required(),
                        Forms\Components\Select::make('goal_id')
                            ->label('Goal')
                            ->options(function ($get) {
                                return Goal::where('category_id', $get('category_id'))->pluck('name', 'id');
                            })
                            ->required(),
                        Forms\Components\TextInput::make('frequency_count')
                            ->type('number')
                            ->suffix('# of hits')
                            ->required(),
                        Forms\Components\TextInput::make('frequency_minutes')
                            ->type('number')
                            ->suffix('minutes')
                            ->required(),
                    ]),
                Forms\Components\Section::make('Actions')
                    ->schema([
                        Forms\Components\Repeater::make('actions')
                            ->addActionLabel('Add Action')
                            ->schema([
                                Forms\Components\Select::make('type')
                                    ->options([
                                        'display_message' => 'Display a message',
                                        'display_image'   => 'Display an image',
                                        'send_email'      => 'Send an email',
                                        'send_sms'        => 'Send an SMS',
                                        'send_webhook'    => 'Send a webhook',
                                    ])
                                    ->reactive()
                                    ->required(),
                                Forms\Components\Fieldset::make('Display Message')
                                    ->visible(fn($get) => $get('type') === 'display_message')
                                    ->schema([
                                        Forms\Components\Textarea::make('message')
                                            ->columnSpanFull()
                                            ->required(),
                                    ]),
                                Forms\Components\Fieldset::make('Display Image')
                                    ->visible(fn($get) => $get('type') === 'display_image')
                                    ->schema([
                                        Forms\Components\FileUpload::make('image')
                                            ->directory('signals')
                                            ->visibility('public')
                                            ->image()
                                            ->helperText('Image should be at least 1200px wide and 9x16 aspect ratio for optimal viewing.'),
                                    ]),
                                Forms\Components\Fieldset::make('Email')
                                    ->visible(fn($get) => $get('type') === 'send_email')
                                    ->schema([
                                        Forms\Components\TextInput::make('email')
                                            ->type('email')
                                            ->required(),
                                        Forms\Components\Textarea::make('message')
                                            ->required(),
                                    ]),
                                Forms\Components\Fieldset::make('SMS')
                                    ->visible(fn($get) => $get('type') === 'send_sms')
                                    ->schema([
                                        Forms\Components\TextInput::make('phone')
                                            ->type('tel')
                                            ->required(),
                                        Forms\Components\Textarea::make('message')
                                            ->required(),
                                    ]),
                                Forms\Components\Fieldset::make('Webhook')
                                    ->visible(fn($get) => $get('type') === 'send_webhook')
                                    ->schema([
                                        Forms\Components\TextInput::make('url')
                                            ->type('url')
                                            ->columnSpanFull()
                                            ->required(),
                                    ]),
                            ])
                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name')
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListSignals::route('/'),
            'create' => Pages\CreateSignal::route('/create'),
            'edit'   => Pages\EditSignal::route('/{record}/edit'),
        ];
    }
}
